package JavaRMI;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * Class for connecting to server and file management
 */
public class ClientHelper {
    private static ServerInterface server;

    /**
     * Connect to server using Registry
     */
    public static ServerInterface connectToServer(int port) {
        try {
            // Connect to the first registered device in the naming service
            Registry namingService = LocateRegistry.getRegistry(port);
            String lookedUpName = namingService.list()[0];
            server = (ServerInterface) namingService.lookup(lookedUpName);
        } catch (RemoteException ex) {
            Logger.getLogger(ClientHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex){
            Logger.getLogger(ClientHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return server;
    }

    /**
     * Open file selection dialog
     */
    public static File chooseFile(){
        // configure file explore dialog
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setAcceptAllFileFilterUsed(false);

        // change texts and type of selection to upload context
        chooser.setDialogTitle("Choose file to upload");
        chooser.setApproveButtonText("Upload");
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        File file = null;

        // Open dialog and verify if chosen option is valid
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        }

        return file;
    }

    /**
     * Returns filename of the selected file
     */
    public static String getFilename(File file){
        JFileChooser chooser = new JFileChooser();
        return chooser.getName(file);
    }

    /**
     * Open and read file
     */
    public static byte[] readFile(String filename){
        byte[] data = {};
        Path fileLocation = Paths.get(filename);

        try {
            data = Files.readAllBytes(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Save data to selected directory
     */
    public static boolean saveFile(String filename, byte[] data){
        // configure file explore dialog
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setAcceptAllFileFilterUsed(false);

        // change texts and type of selection to download context
        chooser.setDialogTitle("Save to directory");
        chooser.setApproveButtonText("Save to");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        // Open dialog and verify if chosen option is valid
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath();

            // write file bytes to chosen path
            try (FileOutputStream fos = new FileOutputStream(path+"/"+filename)) {
                fos.write(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            return false;
        }

        return true;
    }
}

