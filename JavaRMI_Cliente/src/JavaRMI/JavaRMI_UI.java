package JavaRMI;

import External.ListAction;
import javafx.util.Pair;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by leonardo on 24/04/18.
 */
public class JavaRMI_UI {
    private DefaultListModel model;
    private JButton uploadButton;
    private JList list;
    private JPanel panel;
    private JScrollPane scrollpane;
    private static ArrayList<Pair> filesList;
    private static ArrayList<Pair> subscribed;
    private static ClientInterface client;
    private static JFrame frame;
    private static ServerInterface server;

    public JavaRMI_UI() {
        // Upload button listener, if clicked open upload dialog
        uploadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = ClientHelper.chooseFile();
                if (file != null) {
                    JavaRMI_UI.this.showOptionUpload(file);
                }
            }
        });

        // List item selection listener
        Action displayAction = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e){
                // Get the filename of the item selected
                JList item = (JList)e.getSource();
                String filename = item.getSelectedValue().toString();

                // Get the details of the selected file
                String details = "";
                try {
                    /** Call details method at server */
                    details = server.details(filename);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }

                // Check if client is already subscribed to this file
                long timeout = 0L;
                Pair filePair = getSubscriptionPair(filename);
                if(filePair != null) {
                    timeout = Long.parseLong(filePair.getValue().toString());
                }

                // Show different dialog according to file status
                //System.out.println(timeout);
                // If timeout has expired:
                if(System.currentTimeMillis() > timeout) {
                    // As timeout has passed, remove current subscription
                    if(timeout > 0L){ subscribed.remove(filePair); }

                    // if file is available, show download option
                    if (getFileStatus(filename).equals("Available")) {
                        showOptionDownload(filename, details);

                    // if file is not available, show subscribe option
                    } else {
                        showOptionSubscribe(filename, details);
                    }
                }
                // If timeout has not expired, show unsubsribe option
                else{
                    showOptionUnsubscribe(filename, details);
                }
            }
        };

        // Constructor: load list and initialize list listener
        try {
            client = new ClientImpl("client", server, this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        // Create list item selection listener
        ListAction la = new ListAction(list, displayAction);
        // Update list of files
        reloadListModel();
    }

    /**
     * Load the list of files into JList
     */
    private void createUIComponents() {
        // Create a model from ArrayList
        model = new DefaultListModel();
        for(Pair file : filesList){
            model.addElement(file.getKey().toString());
        }

        // Create JList and populate with model
        list = new JList(model);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(12);
        list.setVisible(true);

        // Add a scrollbar to the list
        scrollpane = new JScrollPane(list);
    }

    /**
     * reload list of files
     */
    public void reloadListModel(){
        // Load file list from server
        try {
            filesList = server.consult();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // Add each file in list to model
        DefaultListModel newModel = new DefaultListModel();
        for(Pair file : filesList){
            newModel.addElement(file.getKey());
        }

        // Reload JList from model
        list.setModel(newModel);
    }

    /**
     * Return the file status
     */
    private String getFileStatus(String name){
        String status = "";
        for (Pair p: filesList) {
            if(p.getKey().toString().equals(name)){
                status = p.getValue().toString();
                break;
            }
        }

        return status;
    }

    /**
     * Return the client subscription(filename,timeout) of a file
     */
    private static Pair getSubscriptionPair(String filename){
        Pair pair = null;
        for (Pair s: subscribed) {
            if(filename.equals(s.getKey().toString())){
                pair = s;
                break;
            }
        }

        return pair;
    }

    /**
     * Show a standard message dialog
     */
    private static void showMessage(String message, boolean status, String title) {
        int messageType = status ? 1 : 0;
        JOptionPane.showMessageDialog(frame, message, title,messageType);
    }


    /**
     * Show a option dialog for downloading file
     */
    public static void showOptionDownload(String filename, String details){
        Object[] options = {"Download","Cancel"};

        // Show download dialog
        int res = JOptionPane.showOptionDialog(frame, details, "Download", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        // If download option is selected, then call download method at server
        if(res == 0) {
            try {
                /** Call download method at server */
                byte[] data = server.download(filename);

                // save returned data to file
                boolean result = ClientHelper.saveFile(filename, data);

                // If user choose to save file, then show successful dialog message
                if (result) {
                    Pair filePair = getSubscriptionPair(filename);
                    subscribed.remove(filePair);
                    showMessage(filename + " file was downloaded succesfully.", result, "Download");
                }
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Show a option dialog for subscribe to file
     */
    private void showOptionSubscribe(String filename, String details){
        Object[] options = {"Notify me","Cancel"};
        String message = "If you want to be notified when this\n" +
                         "file becomes available, enter the\n" +
                         "maximum waiting time in seconds bellow.";

        // Show subscribe dialog
        String res = (String)JOptionPane.showInputDialog(
                frame,
                details+"\n\n"+message,
                "Subscribe",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                null);

        // If subscribe option is selected, then call subscribe method at server
        if ((res != null) && (res.length() > 0)) {
            try {
                int seconds = Integer.parseInt(res);
                /** Call subscribe method at server */
                server.subscribe(filename, seconds, client);

                // Add file to the local list of interests
                subscribed.add(new Pair<>(filename,System.currentTimeMillis()+seconds*1000));
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Show a option dialog for unsubscribe to file
     */
    private void showOptionUnsubscribe(String filename, String details){
        Object[] options = {"Unsubscribe","Cancel"};

        // Show dialog
        int res = JOptionPane.showOptionDialog(frame, details, "Subscribe", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        // If unsubscribe option is selected, then call remove subscription
        if(res == 0) {
            try {
                /** Call unsubscribe method at server */
                server.unsubscribe(filename, client);
                Pair filePair = getSubscriptionPair(filename);
                subscribed.remove(filePair);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Show a option dialog for uploading file
     */
    private void showOptionUpload(File file){
        String filename = ClientHelper.getFilename(file);
        String path = file.getPath();
        byte[] data = ClientHelper.readFile(path);

        try {
            /** Call upload method at server */
            boolean result = server.upload(filename, data);

            // Create a result message
            String message = filename;
            message += result ? " uploaded succesfully." : " file is already available in the server!";

            // After upload, update file list by reloading from server
            reloadListModel();

            // Show upload result message (success/fail)
            showMessage(message, result, "Upload");
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            // Try to connect to server
            server = ClientHelper.connectToServer(4567);
            /** Call consult method at server */
            filesList = server.consult();
            // create a local list of interests
            subscribed = new ArrayList<>();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        finally {
            // Instantiate and create GUI frame
            new JavaRMI_UI();
            frame = new JFrame("File sharing");
            frame.setContentPane(new JavaRMI_UI().panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setResizable(false);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }
    }
}
