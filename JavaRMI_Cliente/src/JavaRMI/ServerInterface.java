package JavaRMI;

import javafx.util.Pair;

import java.rmi.*;
import java.util.ArrayList;

/**
 *
 * @author a1722476
 */
public interface ServerInterface extends Remote{
    ArrayList<Pair> consult() throws RemoteException;
    String details(String filename) throws RemoteException;
    byte[] download(String filename) throws RemoteException;
    void subscribe(String filename, int timeout, ClientInterface client) throws RemoteException;
    void unsubscribe(String filename, ClientInterface client) throws RemoteException;
    boolean upload(String filename, byte[] data) throws RemoteException;
}