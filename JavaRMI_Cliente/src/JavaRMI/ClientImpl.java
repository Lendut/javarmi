package JavaRMI;

import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * Implements client interface
 */
public class ClientImpl extends UnicastRemoteObject implements ClientInterface {
    private String name;
    private JavaRMI_UI gui;

    public ClientImpl(String name, ServerInterface s, JavaRMI_UI gui) throws RemoteException{
        this.name = name;
        this.gui = gui;
    }

    /**
     * Open download dialog when server notify that the file is available
     */
    public void notify(String filename, String details) throws RemoteException{
        String message = details + "\n\n" + filename + " file is available now. Do you wish to download it?";
        gui.reloadListModel();
        gui.showOptionDownload(filename, message);
    }
}

