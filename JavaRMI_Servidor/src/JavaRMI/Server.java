package JavaRMI;


import javafx.util.Pair;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author a1722476
 */
public class Server {
    private static ArrayList<Pair> filesList;
    private static ArrayList<Subscription> subscribedFiles;
    private String path;

    public Server(int port, String name) {
        try {
            // Connect
            Registry namingService = LocateRegistry.createRegistry(port);
            ServerImpl serverImpl = new ServerImpl(this);
            namingService.rebind(name, serverImpl);

            // Initialize variables
            filesList = null;
            subscribedFiles = new ArrayList<>();
            getDirectoryFiles();
        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * If file is not found, create a new one. Else, change status from unavailable to available
     */
    public boolean addFile(String filename, byte[] data){
        String status = getFileStatus(filename);

        // If file doesnt exist, create a new one
        if(status.equals("notFound")) {
            // Write to file
            try (FileOutputStream fos = new FileOutputStream(path+"/"+filename)) {
                fos.write(data);
                filesList.add(new Pair<>(filename, "Available"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // If file exists and unavailable, change status
        else if(status.equals("Unavailable")){
            filesList.remove(new Pair<>(filename, "Unavailable"));
            filesList.add(new Pair<>(filename, "Available"));
        }

        // If file exists and is available, do nothing
        else {
            return false;
        }

        return true;
    }

    /**
     * Register client as interested
     */
    public void addSubscription(String filename, int timeout, ClientInterface cliRef){
        Subscription existentSubscribedFile = null;
        long timeNow = System.currentTimeMillis();

        // Loop through list of file with subscriptions and search for filename
        for (Subscription subF:subscribedFiles) {
            if(subF.getFilename().equals(filename)){
                existentSubscribedFile = subF;
                break;
            }
        }

        // If file already has subscription, add client
        if(existentSubscribedFile != null){
            existentSubscribedFile.addInterested(timeNow+timeout*1000, cliRef);
        }
        // If file has no subscriptions, create a new subscription instance
        else{
            subscribedFiles.add(new Subscription(filename, timeNow+timeout*1000, cliRef));
        }
    }

    /**
     * Returns the list of file (name,status) from the server file directory
     */
    public ArrayList<Pair> getDirectoryFiles(){
        if(filesList == null) {
            // Read files in directory ServerFiles
            path = System.getProperty("user.dir") + File.separator + "ServerFiles";
            File[] filesArray = new File(path).listFiles();


            // Initialize variables used in the loop
            filesList = new ArrayList<Pair>();
            String status;
            Random generator = new Random();

            // Add each file in the directory to list of files
            for (File file: filesArray) {
                if (file.isFile()) {
                    // File is available with a probability of 60%.
                    if (generator.nextInt(100) + 1 < 60) {
                        status = "Available";
                    } else {
                        status = "Unavailable";
                    }

                    // Add file to list of files <Name, Status>
                    filesList.add(new Pair<>(file.getName(), status));
                }
            }
        }

        return filesList;
    }

    /**
     * Returns details of a file
     */
    public String getFileDetails(String filename){
        // Load file and initialize attr variable to read its properties
        File file = new File(path+"/"+filename);
        java.nio.file.Path p = Paths.get(path+"/"+filename);
        BasicFileAttributes attr = null;

        // Create string with file details (name,status,creation/access date,size)
        String details = "Name: " + filename;
        details += "\nStatus: " + getFileStatus(filename);
        try {
            attr = Files.readAttributes(p, BasicFileAttributes.class);
            details += "\nCreated at: " + attr.creationTime();
            details += "\nLast accessed at: " + attr.lastAccessTime();

        } catch (IOException e) {
            e.printStackTrace();
        }
        details += "\nSize: " + file.length() + " bytes";

        return details;
    }

    /**
     * Return the status of the file (available,unavailable,notFound)
     */
    private String getFileStatus(String filename){
        String filenameInList;

        // Look for file in list of files and return its status
        for (Pair p: filesList) {
            filenameInList = p.getKey().toString();

            if(filenameInList.equals(filename)){
                return p.getValue().toString();
            }
        }

        // If file is not in the list, return notFound
        return "notFound";
    }

    /**
     * Notify all interested clients
     */
    public void notifyAllSubscribed(String filename){
        // Loop through list of files with subscriptions searching for filename
        Subscription subscribedFile = null;
        for (Subscription subF:subscribedFiles) {
            if(subF.getFilename().equals(filename)){
                subscribedFile = subF;
                break;
            }
        }

        // If file has subscriptions, notify them
        if(subscribedFile != null){
            ArrayList<Pair>subscribed = subscribedFile.getSubscribed();
            String details = getFileDetails(filename);
            long timeNow = System.currentTimeMillis();

            // For each interested client, notify it if timeout has not been reached
            for (Pair subCli: subscribed) {
                long timeoutTime = Long.parseLong(subCli.getValue().toString());
                if(timeNow <= timeoutTime){
                    // Client object reference
                    ClientInterface cliRef = (ClientInterface) subCli.getKey();
                    try {
                        /** Call notify method at client */
                        cliRef.notify(filename, details);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }

            // Eliminate file from list of files with subscriptions
            subscribedFiles.remove(subscribedFile);
        }
    }

    /**
     * Read bytes from file
     */
    public byte[] readFile(String filename){
        byte[] data = {};
        Path fileLocation = Paths.get(path+"/"+filename);

        try {
            data = Files.readAllBytes(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Remove the client from list of interested
     */
    public void removeSubscription(String filename, ClientInterface client){
        // Loop through list of files with subscriptions searching for filename
        Subscription subscribedFile = null;
        for (Subscription subF:subscribedFiles) {
            if(subF.getFilename().equals(filename)){
                subscribedFile = subF;
                break;
            }
        }

        // If file still has subscriptions, then search for client
        if(subscribedFile != null){
            ArrayList<Pair>subscribed = subscribedFile.getSubscribed();

            // Loop through all clients interested in the file
            for (Pair subCli: subscribed) {
                // If caller client is found
                if(subCli.getKey() == client){
                    // Remove client from subscriptions
                    subscribed.remove(client);

                    // If empty, then eliminate file from list of files with subscriptions
                    if(subscribed.isEmpty()){
                        subscribedFiles.remove(subscribedFile);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Create a server instance
     */
    public static void main(String[] args) {
        int port = 4567;
        String name = "//localhost/server";
        Server s = new Server(port, name);
    }
}
