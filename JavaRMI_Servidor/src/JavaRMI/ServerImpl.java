package JavaRMI;


import javafx.util.Pair;

import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 *
 * @author a1722476
 * implements methods directly called by the clients, the logic is called in the Server class
 */
public class ServerImpl extends UnicastRemoteObject implements ServerInterface {
    private Server server;

    public ServerImpl(Server s) throws RemoteException {
        server = s;
    }

    /**
     * Return the list of files in the server
     */
    public ArrayList<Pair> consult() throws RemoteException{
        return server.getDirectoryFiles();
    }

    /**
     * Returns the details of a specific file
     */
    public String details(String filename) throws RemoteException{
        return server.getFileDetails(filename);
    }

    /**
     * Returns the data bytes of a file
     */
    public byte[] download(String filename) throws RemoteException{
        return server.readFile(filename);
    }

    /**
     * Register the client interest
     */
    public void subscribe(String filename, int timeout, ClientInterface client) throws RemoteException{
        server.addSubscription(filename, timeout, client);
    }

    /**
     * Remove the client subscription
     */
    public void unsubscribe(String filename, ClientInterface client) throws RemoteException{
        server.removeSubscription(filename, client);
    }

    /**
     * Add file to server and notify the interested clients
     */
    public boolean upload(String filename, byte[] data) throws RemoteException{
        // Add file if it is not already available
        boolean result = server.addFile(filename, data);

        // If the file was added successfully, notify clients in a new thread
        if(result) {
            new Thread(() -> server.notifyAllSubscribed(filename)).start();
        }

        return result;
    }
}