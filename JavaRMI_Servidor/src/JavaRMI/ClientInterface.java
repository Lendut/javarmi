package JavaRMI;

import java.rmi.*;

/**
 *
 * @author a1722476
 */
public interface ClientInterface extends Remote{
    void notify(String filename, String details) throws RemoteException;
}
