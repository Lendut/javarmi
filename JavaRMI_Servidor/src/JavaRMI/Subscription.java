package JavaRMI;

import javafx.util.Pair;

import java.util.ArrayList;


/**
 * Created by leonardo on 29/04/18.
 * Class of file subscriptions. Each file maintain a list of (client,timeout) interested in it.
 */
public class Subscription {
    private String filename;
    private ArrayList<Pair> interestedList;

    public Subscription(String filename, long timeout, ClientInterface cliRef){
       this.interestedList = new ArrayList<>();
       this.filename = filename;
       interestedList.add(new Pair<>(cliRef, timeout));
    }

    /**
     * Register a new client
     */
    public void addInterested(long timeout, ClientInterface cliRef){
        interestedList.add(new Pair<>(cliRef, timeout));
    }

    /**
     * Returns the filename
     */
    public String getFilename(){
        return this.filename;
    }

    /**
     * Return the list of all interested clients
     */
    public ArrayList<Pair> getSubscribed(){
        return this.interestedList;
    }
}
